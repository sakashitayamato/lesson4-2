class TopController < ApplicationController
    def main
      
    end

    def login
        
    end    

    def create
        @user = User.find_by(uid: session_params[:uid])
        if @user && @user.authenticate(session_params[:password])
            session[:login_uid] = params[:uid]
            redirect_to top_main_path, notice: 'ログインしました'
        else
            render :login
        end
    end
    
    def destroy
        session.delete(:login_uid)
        redirect_to root_path
    end
    
    private
    def session_params
        params.require(:session).permit(:uid, :password)
    end

end
